<?php

if($_REQUEST['param']=='save_book'){
    $wpdb->insert(my_book_table(),array(
        "Name"=>$_REQUEST['name'],
        "Author"=>$_REQUEST['author'],
        "About"=>$_REQUEST['about'],
        "Price"=>$_REQUEST['price']
    ));
    echo json_encode(array("status"=>1,"message"=>"Book is created successfully."));
}
elseif($_REQUEST['param']=='save_author'){
        $wpdb->insert(my_authors_table(),array(
            "Name"=>$_REQUEST['name'],
            "fb_link"=>$_REQUEST['AuthorLink'],
            "About"=>$_REQUEST['about'],
        ));
        echo json_encode(array("status"=>1,"message"=>"Book is created successfully."));
}
elseif($_REQUEST['param']=='edit_book'){
    $wpdb->update(my_book_table(),array(
        "Name"=>$_REQUEST['name'],
        "Author"=>$_REQUEST['author'],
        "About"=>$_REQUEST['about'],
        "Price"=>$_REQUEST['price']),
        array(
            "id"=>$_REQUEST['book_id']
        )
    );
    echo json_encode(array("status"=>1,"message"=>"Book is update successfully."));

}elseif($_REQUEST['param']=='delete_book'){
    
    $wpdb->delete(my_book_table(),array(
            "id"=>$_REQUEST['id']
    ));
    echo json_encode(array("status"=>1,"message"=>"Book deleted successfully."));
}
elseif($_REQUEST['param']=='save_student'){

    $student_id = $user_id = wp_create_user($_REQUEST['username'],$_REQUEST['password'],$_REQUEST['email']);
    $user = new WP_User($student_id);
    $user->set_role("wp_book_user_key");
    $wpdb->insert(my_students_table(),array(
        "Name"=>$_REQUEST['name'],
        "email"=>$_REQUEST['email'],
        "user_login_id"=>$user_id,
    ));
    echo json_encode(array("status"=>1,"message"=>"Student created successfully."));
}

?>  