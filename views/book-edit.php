<?php    
    $book_id = isset($_GET['edit'])?intval($_GET['edit']):0;
    global $wpdb;
    $book_detail = $wpdb->get_row(
        $wpdb->prepare(
            "SELECT * from ".my_book_table()."where id = %d",$book_id
        ),ARRAY_A
    );
?>
<div class = "container"><br>
    <div class = "row">
        <div class ="alert alert-info">
            <h4>Book Update Page</h4>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Update Book Details</div>
            <div class="panel-body">
                <form class="form-horizontal" action="javascript:void(0)" id="frmEditBook">
                    <input type="hidden" name="book_id" value="<?php echo isset($_GET['edit'])?intval($_GET['edit']):0;?>"
                    <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value = "<?php echo $book_detail['name'] ?>" id="name" name="name" required placeholder="Enter name">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="author">Author:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value = "<?php echo $book_detail['author'] ?>" id="author" name="author" required placeholder="Enter author name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="price">Price:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" value = "<?php echo $book_detail['price'] ?>"id="price" name="price" required placeholder="Enter price">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="about">About:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" value = "<?php echo $book_detail['about'] ?>"id="about" name="about" required placeholder="Enter Details"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Update</button>
                        </div>
                    </div>
                </form>              
            </div>
        </div>
    </div>
</div>