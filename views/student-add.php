<div class = "container"><br>
<div class ="alert alert-info">
            <h4>Student Add Page</h4>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Add new Student</div>
            <div class="panel-body">
                <form class="form-horizontal" action="javascript:void(0)" id="frmAddStudent">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Enter name">
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email" name="email" required placeholder="Enter student email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="username">User Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="username" name="username" required placeholder="Enter student username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="password">Password:</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="password" name="password" required placeholder="Enter password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="conf_password">Conform Password:</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="conf_password" name="conf_password" required placeholder="Enter conform password">
                            </div>
                        </div><br>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                    </div>
                </form>              
            </div>
        </div>
</div>