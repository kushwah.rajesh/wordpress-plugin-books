<div class = "container"><br>
    <div class = "row">
        <div class ="alert alert-info">
            <h4>Author Add Page</h4>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Add new Author</div>
            <div class="panel-body">
                <form class="form-horizontal" action="javascript:void(0)" id="frmAddAuthor">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="author">AuthorLink:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="AuthorLink" name="AuthorLink" required placeholder="Enter Author URL">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="about">About:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="about" name="about" required placeholder="Enter Details"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                    </div>
                </form>              
            </div>
        </div>
    </div>
</div>