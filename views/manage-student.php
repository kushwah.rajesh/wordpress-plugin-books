<?php 
    global $wpdb;
    $allstudents = $wpdb->get_results(
        $wpdb->prepare(
            "Select * from ".my_students_table()."",""
        )
    );
?>

<div class="container"><br>
    <div class="alert alert-info">
            <h4>My Student List:</h4>
    </div>
        <div class="panel panel-primary">
            <div class="panel-heading">My Student List</div><br>
                <div class="panel-body">
                    <table id="my-books" class="display" style="width:100%"><br>
                        <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>username</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if(count($allstudents)>0){
                                    $i =1;
                                    foreach($allstudents as $key=>$value){
                                        $userdetails = get_userdata($value->user_login);
                                        ?>
                                        <tr>
                                            <td> <?php echo $i++;?></td>
                                            <td> <?php echo $value->name;?></td>
                                            <td> <?php echo $value->email;?></td>
                                            <td> <?php echo $userdetails->user_login;?></td>
                                            <td> <?php echo $value->created_at;?></td>
                                            <td>
                                                <button class="btn btn-danger">Delete</button>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>