<div class = "container"><br>
    <div class = "row">
        <div class ="alert alert-info">
            <h4>Book Add Page</h4>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">Add new Book</div>
            <div class="panel-body">
                <form class="form-horizontal" action="javascript:void(0)" id="frmAddBook">
                <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" required placeholder="Enter name">
                        </div>
                    </div><div class="form-group">
                        <label class="control-label col-sm-2" for="author">Author:</label>
                        <div class="col-sm-10">
                            <!--<input type="text" class="form-control" id="author" name="author" required placeholder="Enter author name"> -->
                            <select name="author" id="author" class="flow-control">
                                <option value ="-1">----Chose Options----</option>
                                <?php
                                        global $wpdb;
                                        $getallAuthors = $wpdb->get_results(
                                            $wpdb->prepare("select * from ". my_authors_table()."","")
                                        );
                                        foreach($getallAuthors as $index=>$author){
                                            ?>
                                                <option value ="<?php echo $author->id ?>"><?php echo $author->name;?></option>
                                            <?php
                                        }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="price">Price:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="price" name="price" required placeholder="Enter price">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="about">About:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="about" name="about" required placeholder="Enter Details"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>              
            </div>
        </div>
    </div>
</div>