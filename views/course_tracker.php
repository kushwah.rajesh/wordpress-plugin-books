<?php 
    global $wpdb;
    
?>

<div class="container"><br>
    <div class="alert alert-info">
            <h4>My Course Tracker List:</h4>
    </div>
        <div class="panel panel-primary">
            <div class="panel-heading">My Course Tracker List</div><br>
                <div class="panel-body">
                    <table id="my-books" class="display" style="width:100%"><br>
                        <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Name</th>
                                <th>Course</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                                
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div>