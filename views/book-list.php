<?php 
    global $wpdb;
    $all_books = $wpdb->get_results(
                $wpdb->prepare(
                    "SELECT * from ". my_book_table() ,""
                ),ARRAY_A
            );

?>

<div class="container"><br>
    <div class="row">
        <div class="alert alert-info">
            <h4>My Book List:</h4>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">My Book List</div><br>
            <div class="panel-body">
                <table id="my-books" class="display" style="width:100%"><br>
                <thead>
                    <tr>
                        <th>Sr. No</th>
                        <th>Name</th>
                        <th>Author</th>
                        <th>About</th>
                        <th>Price</th>
                        <th>Created at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if(count($all_books)>0){
                            $i = 1;
                            foreach($all_books as $key => $value){
                    ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $value['Name']?></td>
                            <td><?php echo $value['Author']?></td>
                            <td><?php echo $value['About']?></td>
                            <td><?php echo $value['Price']?></td>
                            <td><?php echo $value['Created_at']?></td>
                            <td>
                                <a class="btn btn-info" href="admin.php?page=book-edit&edit=<?php echo $value['id'];?>">Edit</a>
                                <a class="btn btn-danger btnbookdelete" href="javascript:void(0)" data-id="<?php echo $value['id']; ?>">Delete</a>
                            </td>
                        </tr>
                        <?php
                            }
                        }     
                    ?>              
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>