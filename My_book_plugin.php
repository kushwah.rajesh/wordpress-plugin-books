<?php

/**
 * plugin name:my_book_plugin
 * plugin url: https://github.com/
 * Description: Create a Custom Plugin which can contain Book Informations
 * Author:Rajesh Kushwaha
 * Version: 1.0
 */

    if(!defined("ABSPATH")){
        exit();
    }
    if(!defined("MY_BOOK_PLUGIN_DIR_PATH")){
        define("MY_BOOK_PLUGIN_DIR_PATH",plugin_dir_path(__FILE__));
    }
    if(!defined("MY_BOOK_PLUGIN_URL")){
        define("MY_BOOK_PLUGIN_URL", plugins_url()."/My_book_plugin");
    }

    function my_book_include_asserts(){

        $slug='';
        $pages_includes = array("frontendpage","book-list","add-new","add-author","remove-author","add-student","remove-student","course-tracker");

        $currentPage = $_GET['page'];

        if(empty($currentPage) ){
            $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            
            if(preg_match("/my_book/",$actual_link)){
                $slug = "frontend_scripts";
            }
        }

        if(in_array($currentPage,$pages_includes)){

            //style files
            wp_enqueue_style("bootstrap",MY_BOOK_PLUGIN_URL."/asserts/css/bootstrap.min.css",'');
            wp_enqueue_style("datatable",MY_BOOK_PLUGIN_URL."/asserts/css/jquery.dataTables.min.css",'');
            wp_enqueue_style("notifybar",MY_BOOK_PLUGIN_URL."/asserts/css/jquery.notifyBar.css",'');
            wp_enqueue_style("style",MY_BOOK_PLUGIN_URL."/asserts/css/style.css",'');

            //script file 
            wp_enqueue_script("jQuery");
            wp_enqueue_script("bootstrap.min.js",MY_BOOK_PLUGIN_URL."/asserts/js/bootstrap.min.js","",true);
            wp_enqueue_script("jquery.dataTables.min.js",MY_BOOK_PLUGIN_URL."/asserts/js/jquery.dataTables.min.js",'');
            wp_enqueue_script("jquery.notifyBar.js",MY_BOOK_PLUGIN_URL."/asserts/js/jquery.notifyBar.js",'');
            wp_enqueue_script("jquery.validate.min.js",MY_BOOK_PLUGIN_URL."/asserts/js/jquery.validate.min.js",'');
            wp_enqueue_script("script.js",MY_BOOK_PLUGIN_URL."/asserts/js/script.js",'');
            
            wp_localize_script("script.js","mybookajaxurl",admin_url("admin-ajax.php"));
        }
    }
    add_action("init","my_book_include_asserts");


    function my_book_plugin_menus(){
        add_menu_page("My Book","My Book","manage_options","book-list","my_book_list","dashicons-book",30);
        add_submenu_page("book-list","Book List","Book List","manage_options","book-list","my_book_list");
        add_submenu_page("book-list","Add new","Add new","manage_options","add-new","my_book_add");

        add_submenu_page("book-list", "Add New Author", "Add New Author", "manage_options", "add-author", "my_author_add");
        add_submenu_page("book-list", "Manage Author", "Manage Author", "manage_options", "remove-author", "my_author_remove");
        add_submenu_page("book-list", "Add New Student", "Add New Student", "manage_options", "add-student", "my_student_add");
        add_submenu_page("book-list", "Manage Student", "Manage Student", "manage_options", "remove-student", "my_student_remove");
        add_submenu_page("book-list", "Course Tracker", "Course Tracker", "manage_options", "course-tracker", "course_tracker");

        add_submenu_page("book-list","","","manage_options","book-edit","my_book_edit");
    }
    add_action("admin_menu","my_book_plugin_menus");


    function my_book_list(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/book-list.php";
    }

    function my_book_add(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/book-add.php";
    }

    function my_book_edit(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/book-edit.php";
    }
    function my_author_add(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/author-add.php";
    }
    function my_author_remove(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/manage-author.php";
    }
    function my_student_add(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/student-add.php";
    }
    function my_student_remove(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/manage-student.php";
    }
    function course_tracker(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/course_tracker.php";
    }


    function my_book_table(){
        global $wpdb;
        return $wpdb->prefix . 'my_books';       
    }
    function my_authors_table(){
        global $wpdb;
        return $wpdb->prefix . 'my_authors';
    }
    function my_students_table(){
        global $wpdb;
        return $wpdb->prefix . 'my_students';
    }
    function my_enrol_table(){
        global $wpdb;
        return $wpdb->prefix . 'my_enrol';
    }
    register_activation_hook( __FILE__, 'my_book_generates_table_script');
    
    function my_book_generates_table_script() {
            global $wpdb;
            $charset_collate = $wpdb->get_charset_collate();
            $table_name = $wpdb->prefix .my_book_table();
            $sql = "CREATE TABLE `".my_book_table()."` (
            `id` int(11) NOT NULL auto_increment,
            `Name` varchar(220) DEFAULT NULL,
            `Author` varchar(220) DEFAULT NULL,
            `About` varchar(250),
            `Price` int(10),
            `Created_at` timestamp not null default null default current_timestamp,
            PRIMARY KEY(id)
            )ENGINE=MyISAM DEFAULT CHARSET=latin1;
        ";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }

        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix .my_authors_table();
        $sql2 = " CREATE TABLE `".my_authors_table()."` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) DEFAULT NULL,
            `fb_link` text,
            `about` text,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1
        ";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql2);
        }

        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix .my_students_table();
        $sql3 = "
            CREATE TABLE `".my_students_table()."` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) DEFAULT NULL,
            `email` varchar(255) DEFAULT NULL,
            `user_login_id` int(11) DEFAULT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1
        ";           
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql3);
        }
        $sql4 = "
        CREATE TABLE `".my_enrol_table()."` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `student_id` int(11) NOT NULL,
            `book_id` int(11) NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1
        ";  
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql4);
        }

        add_role("wp_book_user_key","MY Book User",array(
            "read"=>true
        ));

        /* Create dynamic page */

        $my_post=array(
            'post_title' => "Book page",
            'post_content' => '[book_page]',
            'post_status' => "publish",
            "post_type" => "page",
            'post_name' => "my_book"
        );
        $book_id = wp_insert_post($my_post);
        add_option("my_book_page_id",$book_id);
    }


    register_activation_hook(__FILE__,"my_book_generates_table_script");

    function my_book_page_functions(){
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/views/my_books_frontend-books-template.php";
    }

    add_shortcode("book_page","my_book_page_functions");

    register_deactivation_hook(__FILE__,'drop_table_plugin_books');
   // register_deactivation_hook(__FILE__,'drop_table_plugin_books');

    function drop_table_plugin_books(){
        global $wpdb;
        $wpdb->query("Drop table if exists ".my_book_table());
        $wpdb->query("Drop table if exists ".my_authors_table());
        $wpdb->query("Drop table if exists ".my_students_table());
        $wpdb->query("Drop table if exists ".my_enrol_table());

        if(get_role("wp_book_user_key")){
            remove_role("wp_book_user_key");
        }

        //delete page

        if(!empty(get_option("my_book_page_id"))){
            $post_id = get_option("my_book_page_id");
            wp_delete_post($post_id, true);
            delette_option("my_book_page_id");
        }
    }


    add_action("wp_ajax_mybooklibrary","my_book_ajax_handler");

    function my_book_ajax_handler(){
        global $wpdb;
        include_once MY_BOOK_PLUGIN_DIR_PATH ."/library/mybooklibrary.php";

        wp_die();
    }


    add_filter("page_template","owt_custom_page_layout");


    function owt_custom_page_layout($page_template){
        global $post;
        $page_slug = $post->post_name;
        if($page_slug == "my_book"){
            $page_template = MY_BOOK_PLUGIN_DIR_PATH . '/views/frontend-books-template.php';
        }
        return $page_template;
    }
?>

