jQuery(document).ready(function() {

    jQuery("#frmAddStudent").validate({
        submitHandler:function(){   
            var postdata = "action=mybooklibrary&param=save_student&"+jQuery("#frmAddStudent").serialize();     
            jQuery.post(mybookajaxurl,postdata,function(response){
                var data = jQuery.parseJSON(response);
                //console.log(data.status);
                if(data.status === 1){
                    jQuery.notifyBar({
                        cssClass:"success",
                        html:data.message,
                    });
                    setTimeout(function(){
                        //window.location.reload();
                        location.reload();
                    },1300)
                }else{

                }
            });   
        }
    });

    jQuery("#frmAddAuthor").validate({
        submitHandler:function(){   
            var postdata = "action=mybooklibrary&param=save_author&"+jQuery("#frmAddAuthor").serialize();     
            jQuery.post(mybookajaxurl,postdata,function(response){
                var data = jQuery.parseJSON(response);
                //console.log(data.status);
                if(data.status === 1){
                    jQuery.notifyBar({
                        cssClass:"success",
                        html:data.message,
                    });
                    setTimeout(function(){
                        //window.location.reload();
                        location.reload();
                    },1300)
                }else{

                }
            });   
        }
    });

    jQuery('#my_books').DataTable();
    jQuery(document).on("click",".btnbookdelete",function(){
        var conf = confirm("Are you sure you want to delete this book?");
        if(conf){
            var book_id = jQuery(this).attr("data-id");
        var postdata = "action=mybooklibrary&param=delete_book&id="+book_id;     
            jQuery.post(mybookajaxurl,postdata,function(response){
                var data = jQuery.parseJSON(response);
                //console.log(data.status);
                if(data.status === 1){
                    jQuery.notifyBar({
                        cssClass:"success",
                        html:data.message,
                    });
                    setTimeout(function(){
                        //window.location.reload();
                        location.reload();
                    },1300)
                }else{

                }
            });
        }
    });

    jQuery("#frmAddBook").validate({
        submitHandler:function(){   
            var postdata = "action=mybooklibrary&param=save_book&"+jQuery("#frmAddBook").serialize();     
            jQuery.post(mybookajaxurl,postdata,function(response){
                var data = jQuery.parseJSON(response);
                //console.log(data.status);
                if(data.status === 1){
                    jQuery.notifyBar({
                        cssClass:"success",
                        html:data.message,
                    });
                    setTimeout(function(){
                        //window.location.reload();
                        location.reload();
                    },1300)
                }else{

                }
            });   
        }
    });

    
    jQuery("#frmEditBook").validate({
        submitHandler:function(){    
            var postdata = "action=mybooklibrary&param=edit_book&"+jQuery("#frmEditBook").serialize();     
            jQuery.post(mybookajaxurl,postdata,function(response){
                var data = jQuery.parseJSON(response);
                //console.log(data.status);
                if(data.status === 1){
                    jQuery.notifyBar({
                        cssClass:"success",
                        html:data.message,
                    });
                }else{

                }
            });                    
        }
    });
});
